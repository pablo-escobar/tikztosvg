# tikztosvg(1)

**WARNING**: This repository was moved to https://git.sr.ht/~pablo-pie/tikztosvg

The `tikztosvg(1)` command renders TikZ diagrams to SVG, using
[XeTeX](http://xetex.sourceforge.net/) and
[pdf2svg](https://github.com/dawbarton/pdf2svg).

## Usage

``` sh
$ tikztosvg [OPTION]... INPUT_PATH
```

If *INPUT\_PATH* is set to *-* the input will be read from stdin.

### Options

**-o, --output**=*OUTPUT\_PATH*  
Write output to file *OUTPUT\_PATH*. If set to *-* the output will be written
to stdout. Defaults to the base name of the input file suffixed with the *.svg*
extension. The file is resolved relative to the working directory.

**-p, --package**=+*PACKAGE*  
Include `\usepackage{PACKAGE}` when rendering the diagram. The tikz, tikz-cd,
pgfplots, amsmath and amssymb packages are always included by default.

**-l, --library**=+*LIBRARY*
Include `\usetikzlibrary{LIBRARY}` when rendering the diagram.

**-q, --quiet**  
Silence application log messages and script warnings.

**-h, --help**  
Print a help message.

**-v, --version**  
Print version information.

## Example

Let’s say you want to convert the file `example.tikz` to SVG:

``` latex
\begin{tikzcd}
     G \arrow[r, "\varphi"] \arrow[d, "\psi"', two heads] & H \\
  \mathlarger{\sfrac{G}{\ker \varphi}} \arrow[ru, dotted] &
\end{tikzcd}
```

You could achieve this by running:
``` sh
# xfrac and relsize are only there so that we can 
# call \sfrac and \mathlarger
$ tikztosvg -p xfrac -p relsize example.tikz
```

The results will be stored in the file `example.svg`.

## Installation

The `tikztosvg(1)` command and it’s man page can be installed in Unix systems
by running

``` sh
$ curl -s https://git.sr.ht/~pablo-pie/tikztosvg/blob/master/install.sh | sudo sh
```

or it can be installed via Git with

``` sh
$ git clone https://gitlab.com/pablo-escobar/tikztosvg.git
$ cd tikztosvg
$ sudo make install
```

The executable is installed in `$HOME/.local/bin/` and the man-page is
installed in `$HOME/.local/share/man/man1/`.

## Authors

**tikztosvg** was written by [Pablo](mailto:pablo-escobar@riseup.net).

**pdf2svg** was written by [David Barton](mailto:davebarton@cityinthesky.co.uk)
and [Matthew Flaschen](mailto:matthew.flaschen@gatech.edu).

## License

© 2021 Pablo.

Free use of this software is granted under the terms of the GPL-3.0 License.
