.PHONY: package install

# Packages the application for CTAN
package: tikztosvg.tar.gz

install: man/tikztosvg.1 tikztosvg
	# Install the executable
	cp tikztosvg ~/.local/bin/
	chmod +x ~/.local/bin/tikztosvg
	 
	# Install the manpage
	install -g 0 -o 0 -m 0644 man/tikztosvg.1 ~/.local/share/man/man1/

man/tikztosvg.1: man/man.adoc
	asciidoctor -b manpage -o $@ $^

tikztosvg.pdf: README.md 
	pandoc -s -o $@ $^

example/example.svg: example/example.tikz
	sh ./tikztosvg -p xfrac -p relsize -o $@ $^
	
	# Try to compress the image with svgo
	svgo --pretty --enable=sortAttrs $@ || exit 0

# Packages the application for CTAN
tikztosvg.tar.gz: man/tikztosvg.1 tikztosvg.pdf README.md example/example.svg CHANGELOG.md
	tar -cvO --directory=.. --exclude='.*' --exclude='*.tar.gz' tikztosvg \
		| gzip -c /dev/stdin > tikztosvg.tar.gz

