# CHANGELOG

## Release 0.2.0

* Removed code that added the `\huge` command to the beginning of the document
* Added an option for importing TikZ libraries (the `-l` option)
* Made it so that items from the `-p` and `-l` options aren't imported twice
* Made it so that tikz, tikz-cd, pgfplots, amsmath and amssymb are imported by
  default
* Added instructions for installing the application with Git

## Release 0.1.2

* The script is now posix-complient (it doesn't use bash anymore)
* Messages in the log output are now color-coded
* The script now support `stdin` input and `stdout` output
* The documentation is now rendered to PDF
* The documentation was updated
